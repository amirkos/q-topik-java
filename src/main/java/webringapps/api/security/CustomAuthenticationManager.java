package webringapps.api.security;

import java.util.Iterator;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationManager implements AuthenticationProvider {

	@Override
	public Authentication authenticate(Authentication arg0) throws AuthenticationException {
		List<?> arr = (List) arg0.getAuthorities();
		for (Iterator iterator = arr.iterator(); iterator.hasNext();) {
			GrantedAuthority object = (GrantedAuthority) iterator.next();
			System.out.println(object.getAuthority());
			
		}
		return null;
	}

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}
