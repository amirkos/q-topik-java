package webringapps.api.security;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
/**
 * 
 * @author hraaks
 *
 *
 */
@Configuration

@ComponentScan("webringapps.api.security")
@ImportResource({ "classpath:security/webSecurityConfig.xml" })


public class SecurityXmlConfig {

 public SecurityXmlConfig() {
	
     super();
     System.out.println("SecurityXmlConfig");
     
 }

}
