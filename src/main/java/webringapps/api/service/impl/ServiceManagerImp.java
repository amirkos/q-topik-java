package webringapps.api.service.impl;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import webringapps.api.dao.RestServiceDAOImpl;
import webringapps.api.model.InquriesSummary;
import webringapps.api.model.Profile;
import webringapps.api.model.Topic;
import webringapps.api.model.User;
import webringapps.api.service.ServiceManager;


/**
 * @author hraaks
 */

@Repository
public class ServiceManagerImp implements ServiceManager {
	
	@Autowired
	private RestServiceDAOImpl dao;


	@Override
	public List<Profile> getAllProfiles() {
		
		return dao.getAllProfiles();
	}


	@Override
	public Profile createProfile(Profile p) {
		return dao.createProfile(p);
		
	}
	
	@Override
	public Profile updateProfile(Profile p) {
		return dao.updateProfile(p);
	}


	@Override
	public Profile findProfileById(String id) {
		Profile profile = dao.getProfile(new Long(id).longValue());
		 if (profile == null) {
	            throw new EntityNotFoundException("Can't Topic Profile for ID "
	                    + id);
	        }		
		return profile;
	}


	@Override
	public Topic createTopic(Topic t) {
//		Topic topic = getTopicById(t.getId().toString());

		t.setId(null);
		t.setCreated(new Timestamp(System.currentTimeMillis()));
    	t.setLastModified(new Timestamp(System.currentTimeMillis()));
		InquriesSummary summary = new InquriesSummary();
		t.setSumary(summary);
		t.getInquries().forEach(k->{
        	k.setId(null);
        	k.setTopic(t);
        });
		
    	

		return dao.createTopic(t);
	}


	@Override
	public Topic getTopicById(String id) {

		return dao.getTopic(new Long(id));
	}


	@Override
	public int getTopicVotes(String topic_id) {
		Topic t = dao.getTopic(new Long(topic_id).longValue());
		
		return t.getSumary().getCount().intValue();
	}


	@Override
	public Topic createTopicVotes(String topic_id) {  
		return dao.createTopicVote(new Long(topic_id).longValue());
	}   


	public byte [] setInquiryImage(String topicId, String InquiryId, byte[] payload ) throws IOException {
		
		return dao.setInquiryImage(new Long(topicId).longValue(), new Long(InquiryId).longValue(), payload);
		
	}


	@Override
	public User getUserById(String user_id) {
		User user = dao.getUser(user_id);
		
		return user;
	}
	
	@Override
	public List getUsers() {
		return dao.getUsers();
	}


	
	@SuppressWarnings("unchecked")
	public List<Topic> getAllTopics() {
		// TODO Auto-generated method stub
		return dao.getAllTopics();
	}


	

}
