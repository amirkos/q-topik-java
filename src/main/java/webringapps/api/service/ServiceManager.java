package webringapps.api.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import webringapps.api.model.Profile;
import webringapps.api.model.Topic;
import webringapps.api.model.User;

/**
 * @author hraaks
 */

public interface ServiceManager {
	
	public List<Profile> getAllProfiles();
	
	public Profile createProfile(Profile p);
	
	public Profile updateProfile(Profile p);
	
	public Profile findProfileById(String id);
	
	public Topic createTopic(Topic t);
	
	public Topic getTopicById(String id);
	
	public List<Topic> getAllTopics();
	
	public int getTopicVotes(String topic_id);
	
	public Topic createTopicVotes(String topic_id);
	
	public List getUsers();
	
	public byte [] setInquiryImage(String topicId, String InquiryId, @RequestBody byte[] payload ) throws IOException;
	
	
	public User getUserById(String user_id);
	

}
