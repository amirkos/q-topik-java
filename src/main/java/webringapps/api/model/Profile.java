package webringapps.api.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Profile")
public class Profile {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private String avatarUrl;
	private String nickname;
	private String phone;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Timestamp getLastModfied() {
		return lastModfied;
	}
	public void setLastModfied(Timestamp lastModfied) {
		this.lastModfied = lastModfied;
	}
	private Timestamp created;
	private Timestamp lastModfied;
	
	
	
	public Profile( String user_name) {
		this.username = user_name;
		
	}
	
	public Profile() {
		
	}
	@Override
	public String toString() {
		StringBuffer buffy  = new StringBuffer();
		buffy.append("Id:" + id);
		buffy.append("\nusername:" + username);
		buffy.append("\nfirstName:" + firstName);
		buffy.append("\nlastName:" + lastName);
		buffy.append("\nnickname:" + nickname);
		buffy.append("\nphone:" + phone);
		buffy.append("\ncreated:" + created);
		buffy.append("\nlastModfied:" + lastModfied);
		
	    
		return buffy.toString();
	}

	


}
