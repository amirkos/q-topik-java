package webringapps.api.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity(name = "Topic")
@Table(name = "topic")
public class Topic {

	@Id
	@GeneratedValue
	
	private Long id;

	private String name;
	private String category;
	
	 @OneToMany(
		        mappedBy = "topic", 
		        cascade = CascadeType.ALL, 
		        orphanRemoval = true
		    )
    @JsonProperty("inquries")
	@JsonIgnoreProperties 
	private List<Inquries> inquries  = new ArrayList<>();
	 
	 @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	private Timestamp created;
	 @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	 private Timestamp lastModified;
	
	public Topic() {
		
	}
	
	
	@JsonIgnoreProperties
	@OneToOne (cascade=CascadeType.ALL)
	@JoinColumn(name = "inquiryId")
	@JsonProperty("InquriesSummary")
	
	private InquriesSummary sumary;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<Inquries> getInquries() {
		return inquries;
	}

	public void setInquries(List<Inquries> inquries) {
		this.inquries = inquries;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastModified() {
		return lastModified;
	}

	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}

	public InquriesSummary getSumary() {
		return sumary;
	}

	public void setSumary(InquriesSummary sumary) {
		this.sumary = sumary;
	}

	
	
}
