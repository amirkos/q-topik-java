package webringapps.api.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "Inquries")
@Table(name = "topic_inquries")
public class Inquries {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "topic_id")
	@JsonIgnore
	
	private Topic topic;
	
	private String name;
	private Integer inquiryOrder;
	private String description;
	
	@Lob
    @Column(name = "image", columnDefinition="BLOB")
	@JsonIgnore
	private byte [] image;
	private String imageUrl;
	private Integer numericValue;
	 @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	private Timestamp created;
	 @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	private Timestamp lastModified;
	
	
	public Inquries() {
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public Integer getInquiryOrder() {
		return inquiryOrder;
	}

	public void setInquiryOrder(Integer inquiryOrder) {
		this.inquiryOrder = inquiryOrder;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public Integer getNumericValue() {
		return numericValue;
	}
	public void setNumericValue(Integer numericValue) {
		this.numericValue = numericValue;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Timestamp getLastModified() {
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}
	
	
	

}
