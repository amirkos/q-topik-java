package webringapps.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity(name = "InquriesSummary")
@Table(name = "inq_summary")
public class InquriesSummary {
	
	@Id
	@GeneratedValue
	
	private Long inquiryId;
	private Integer count;


	
	 public InquriesSummary() {
		 count = new Integer(0);
		// TODO Auto-generated constructor stub
	}
		
	
	
	public Long getInquiryId() {
		return inquiryId;
	}
	public void setInquiryId(Long inquiryId) {
		this.inquiryId = inquiryId;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count += count;
	}
	

}
