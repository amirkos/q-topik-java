package webringapps.api.controlers;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import webringapps.api.model.*;

import webringapps.api.service.impl.ServiceManagerImp;

@RestController
public class RestControler {    
	

	    @Autowired
	    ServiceManagerImp manager;
	    
	    private static final Logger logger   = LoggerFactory.getLogger(RestControler.class);

	    
	    /**
	     * 
	     * @param creates or updates profile
	     * @return Profile json
	     * @throws Exception
	     */
	
	    @RequestMapping(value = "/profile", method = RequestMethod.PUT, headers = "Accept=application/json")
	    public Profile createProfile( @RequestBody String payload)  throws Exception {
	    	ObjectMapper maper = new ObjectMapper();
	    	logger.info("/profile " + payload );
	    	Profile p = new Profile();
	    	p = maper.readValue(payload, Profile.class);   
	    	Profile ret = manager.findProfileById(p.getId().toString());
	    	if(ret == null) {
	    		ret = manager.createProfile(p);
	    	}else {
	    		ret = manager.updateProfile(p);
	    	}
	    	return ret;
	    	  
	    }
	    
	   
	    
	    /**
	     * 
	     * @param id
	     * @return Profile
	     * @throws Exception
	     */
	    
	    @RequestMapping(value = "/profile/{id}", method = RequestMethod.GET )
	    public Profile getProfileById(  @PathVariable("id") String id)  throws Exception{

	        return manager.findProfileById(id);
	    }
	    
	    /**
	     * 
	     * @return
	     */
	    
	    @RequestMapping(value = "/profiles", method = RequestMethod.GET)
	    public List<Profile> getProfiles() {	    	
	    	return manager.getAllProfiles();  
	    }
	    
	    /**
	     * 
	     * @return
	     */
	    @RequestMapping(value = "/users", method = RequestMethod.GET)
	   	
	    public List<User> getAllUsers() {
	    	return manager.getUsers();
	    	
	    }
	    /**
	     * 
	     * @param id
	     * @return
	     */
	    
	    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	    @ResponseBody
	    public User getUserById(  @PathVariable("id") String id) {
	     
	    	logger.info("\"Returning a user with id =\" + id;");
	        return manager.getUserById(id);
	    }
	    
	    /**
	     * 
	     * @return
	     */
	    
	    @RequestMapping(value = "/topics", method = RequestMethod.GET)
	   	
	    public List<Topic> getAllTopics() {
	    	return manager.getAllTopics();
	    }
	    /**
	     * 
	     * @param payload
	     * @return
	     */
	    
	    @RequestMapping(value = "/topics", method = RequestMethod.POST, headers = "Accept=application/json")
	    
	    public String createTopic(@Valid @RequestBody String payload) throws Exception{
	    	
	    	ObjectMapper maper = new ObjectMapper();
	    	maper.setSerializationInclusion(Include.ALWAYS);
	    	Topic _topic = new Topic();
	    	_topic = maper.readValue(payload, Topic.class);	    	
	    	String json = maper.writeValueAsString(manager.createTopic(_topic));
	    	logger.info("Topic created:" + json);
	    	return json;
	    	
	    }
	    /**
	     * 
	     * @param ex
	     * @return
	     */
	    
	    @ExceptionHandler(NullPointerException.class)
	    @ResponseBody
	    public String handleException1(NullPointerException ex)
	    {
	        return ex.getMessage();
	    }
	    
	    /**
	     * 
	     * @param id
	     * @return
	     */
	    @RequestMapping(value = "/topics/{id}", method = RequestMethod.GET )
	 	
	    public Topic getTopicById( @PathVariable("id") String id) {
	    	logger.info("Returning a TOPIC with id =" + id);
	    	Topic t = manager.getTopicById(id);

	    	ObjectMapper maper = new ObjectMapper();  
	    	maper.setSerializationInclusion(Include.ALWAYS);
	    	String jsonInString = null;
	    	try {
	    		jsonInString = maper.writerWithDefaultPrettyPrinter().writeValueAsString(t);     
			} catch (Exception e) {
				System.out.println(e.toString());
				
			}
	    	logger.info(jsonInString);
	    	
	    	
	    	
	    	return t;
	    			 
	    	
	    }
	    
	    
	    /**
	     * Set inquiry image
	     * @param id
	     * @return
	     */
	    @RequestMapping(value = "/topics/{topicId}/inquiry/{InquiryId}/image", 
	    		method = RequestMethod.POST)
	    	
	    public byte [] setImage( @PathVariable String topicId, @PathVariable String InquiryId, @RequestBody byte [] payload ) throws IOException{
	    	
	    	 System.out.println("Returning a topic with id =" + topicId + " with inquiry id:" + InquiryId);

	         manager.setInquiryImage(topicId, InquiryId, payload);

	    	 return payload	;    	
	    }
	    
	    
	    /**
	     * get topic votes
	     * @param id
	     * @return
	     */
	    @RequestMapping(value = "/topics/{id}/votes", 
	    		method = RequestMethod.GET)
	  		
	    public Integer getTopicVotes( @PathVariable("id") String id) {  
	    	 logger.info("\"Returning a topic with id =\" "+ id);
	    	 int res = manager.getTopicVotes(id); 
	    	 logger.info("votes:" + res);
	    	 
	    	 return  new Integer(res);    
	    	
	    }
	    
	    /**
	     * get topic votes
	     * @param id
	     * @return
	     */
	    @RequestMapping(value = "/topics/{id}/votes", 
	    		method = RequestMethod.POST)  
	  	
	    public Topic setTopicVotes( @PathVariable("id") String id) {
	    	 logger.info("\"Returning a topic with id =\" + id;");
	    	 Topic topic = manager.createTopicVotes(id);  
	    	 return topic;
	    	
	    }
	    
	    @RequestMapping(value = "/logout", method = RequestMethod.GET)
	    @ResponseBody		
	    public void LogOut( HttpSecurity http) throws Exception{     
	    	 System.out.println("tu sam dosooooooo");
	    	 http.logout();
	    	
	    	
	    }
	    
	    
	    

}
