package webringapps.api.dao;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import webringapps.api.controlers.RestControler;
import webringapps.api.model.*;

@Repository
@Transactional
public class RestServiceDAOImpl implements RestServiceDAO {

	@PersistenceContext
	private EntityManager entityManager;

	private static final Logger logger = LoggerFactory.getLogger(RestServiceDAOImpl.class);

	/**
	 * // Session session = entityManager.unwrap(Session.class); get session manager
	 * from entity manager hibernate specific
	 */

	@Override
	public Profile createProfile(Profile p) {
		logger.info("***createPofile***");
		p.setId(null);
		p.setCreated(new Timestamp(System.currentTimeMillis()));
		p.setLastModfied(new Timestamp(System.currentTimeMillis()));
		entityManager.persist(p);
		return p;

	}

	@Override
	public Profile updateProfile(Profile p) {
		Profile profile = entityManager.find(Profile.class, p.getId());
		profile.setAvatarUrl(p.getAvatarUrl());
		profile.setEmail(p.getEmail());
		profile.setFirstName(p.getFirstName());
		profile.setLastName(p.getLastName());
		profile.setNickname(p.getNickname());
		profile.setPhone(p.getPhone());
		profile.setNickname(p.getNickname());
		entityManager.persist(profile);
		logger.info("Profile updated!");
		return profile;
	}

	@Override
	public List<Profile> getAllProfiles() {

		String hql = "FROM Profile";
		Query query = entityManager.createQuery(hql);
		List<Profile> results = query.getResultList();
		return results;
	}

	@Override
	public Profile getProfile(long id) {
		return entityManager.find(Profile.class, new Long(id));
	}

	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Topic createTopic(Topic t) {
		entityManager.persist(t);
		return t;
	}

	@Override
	public Topic getTopic(long id) {
		// TODO Auto-generated method stub
		Topic topic = entityManager.find(Topic.class, new Long(id));
		if (topic == null) {
			throw new EntityNotFoundException("Can't find Topic  for ID " + id);
		}

		return topic;
	}

	@Override
	public Topic createTopicVote(long topic_id) {
		Topic t = entityManager.find(Topic.class, new Long(topic_id));
		t.getSumary().setCount(1);
		entityManager.persist(t);
		return t;

	}

	@Override
	public int getTopicVote(long topic_id) {
		Topic t = entityManager.find(Topic.class, new Long(topic_id));
		if (t == null)
			throw new EntityNotFoundException("Cant find topic with ID:" + topic_id);
		return t.getSumary().getCount();

	}

	@Override
	public byte[] setInquiryImage(long topic_id, long inquiry_id, @RequestBody byte[] image) throws IOException {

		Topic t = getTopic(topic_id);
		
		Inquries inquries = entityManager.find(Inquries.class, new Long(inquiry_id));
		if(inquries == null ) throw new EntityNotFoundException("Cant find inquiry with id:"+inquiry_id);
//		inquries.setImage(image);

		System.out.println("" + getClass().getResource("."));
		String rootPath = System.getProperty("user.dir");
		System.out.println(rootPath);
		String file_name = rootPath + "\\.content\\images\\" + "image_" + inquiry_id + ".jpg";
		inquries.setImageUrl(file_name);

		FileOutputStream fos = new FileOutputStream(file_name);
		fos.write(image);
		fos.flush();

		fos.close();

		entityManager.persist(inquries);
		return image;

	}

	@Override
	public User getUser(String id) {
		// TODO Auto-generated method stub
		User user = entityManager.find(User.class, new Long(id));
		if (user == null) {
			throw new EntityNotFoundException("Can't find user for ID " + id);
		}

		return user;

	}

	@Override
	public List<User> getUsers() {
		String hql = "FROM User";
		Query query = entityManager.createQuery(hql);
		List<User> results = query.getResultList();
		if (results.size() == 0) {
			throw new EntityNotFoundException("No users in DB!");
		}

		return results;
	}

	@Override
	public List getAllTopics() {
		String hql = "FROM Topic";
		Query query = entityManager.createQuery(hql);
		List<Topic> results = query.getResultList();
		if (results.size() == 0) {
			throw new EntityNotFoundException("No Topics in DB!");
		}

		return results;
	}

}
