package webringapps.api.dao;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;

import webringapps.api.model.Profile;
import webringapps.api.model.Topic;
import webringapps.api.model.User;



public interface RestServiceDAO {
	
	Profile createProfile(Profile p);
	
	Profile updateProfile(Profile p);
	
	List <Profile> getAllProfiles();
	
	Profile getProfile(long id);
	
    void delete(long id);
    
    Topic createTopic(Topic t);
    
    Topic getTopic(long id);
    
    List<Topic> getAllTopics();
    
    List<User> getUsers();
    
    User getUser(String id);
    
    Topic createTopicVote(long topic_id);
    
    int getTopicVote(long topic_id);
    
    byte [] setInquiryImage(long topic_id,long inquiry_id, @RequestBody byte[] image) throws IOException;
       
    
    
    

}
