package webringapps.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ImportResource;


@SpringBootApplication

@ImportResource (value = { 
	    "classpath:/hsql/hsql_cfg.xml"
		,
	    "classpath:/hsql/data_source.xml"

	    })

@EntityScan(basePackages = { 
		"webringapps.api.model",
		"webringapps.api.dao",
		"webringapps.api.controlers",
		"webringapps.api.validators"
		,
		"webringapps.api.security"
		,
		"webringapps.api.error"
		})

public class RestApp {
	public static void main(String[] args) {
		SpringApplication.run(RestApp.class, args);
	}
	
//	@Bean
//	BasicAuthenticationFilter basicAuthFilter(AuthenticationManager authenticationManager,
//			AuthenticationEntryPoint basicAuthEntryPoint) {
//		return new BasicAuthenticationFilter(authenticationManager, basicAuthEntryPoint());
//	}
//	
//	@Bean
//	AuthenticationEntryPoint basicAuthEntryPoint() {
//		RestAuthenticationEntryPoint bauth = new RestAuthenticationEntryPoint();
//		//bauth.setRealmName("GAURAVBYTES");
//		return bauth;
//	}
//	
//	@Bean
//	FilterRegistrationBean customSecurityHeaderFilterRegistrationBean() {
//		FilterRegistrationBean filterRegistration = new FilterRegistrationBean();
//		filterRegistration.setFilter(new CustomSecurityHeaderFilter());
//		filterRegistration.setOrder(Ordered.HIGHEST_PRECEDENCE);
//		return filterRegistration;
//	}
}
